/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function clickMe()
{
    var fname = $("#first_name").val();  //get the id of first name 
    var lname = $("#last_name").val();    //get the id of last name
    var email = $("#email").val();        //get the id of email id
    var pass = $("#pass").val();           //get the id of password 
    var Cpass = $("#con_pass").val();       //get the id of confirm password
    var PNo = $("#phone_No").val();  // get the id of phone number
    var dat = $("#dateofbirth").val();
    var addV = $("#address").val();
    var checkNameexp = /^[a-zA-Z]{2,20}$/;      
    var checkNameexp1 = /^[a-zA-Z]{2,20}$/;  // validation for the names     
    var phoneexp = /^\(?(\d{3})\)?[-]?(\d{3})[-]?(\d{4})$/;                      //validation for the phone number according to US format
    var datexp = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/; // validation for the date 

    var counter = 0;
    //check the first name is valid or invalid

    if (fname !== "" || fname !== null) {
        if (checkNameexp.test(fname)) {
            $("#fst_show").hide();
            counter = counter + 1;
        }
        else {
            $("#fst_show").html("first name is invalid");
            $("#fst_show").show();
            counter = counter - 1;
        }
    }
    
    if (lname !== "" || lname !== null) {  
       //check the last name is valid or invalid
        if (checkNameexp1.test(lname)) {
            $("#lst_show").hide();
            counter = counter + 1;
        }
        else {
            $("#lst_show").html("last name is invalid");
            $("#lst_show").show();
            counter = counter - 1;
        }
    }
    //check the address is valid or invalid
    if (addV === "" || addV === null) {
        $("#add_show").html("address is invalid");
        $("#add_show").show();
        counter = counter - 1;
    }
    else
    {
        $("#add_show").hide();
        counter = counter + 1;
    }
    if (email !== "" || email !== null) {                     // check the email  address after the @ one charater is necessary and after the . two letter necessary   {
        var atposition = email.indexOf("@");
        var dotposition = email.lastIndexOf(".");
        if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= email.length) {
            $("#email_show").html("email is invalid");
            $("#email_show").show();
            counter = counter - 1;
        }
        else {
            $("#email_show").hide();
            counter = counter + 1;
        }
    }
    if (pass !== "" || pass !== null) {
        if (pass.length > 6) {                  //password length is greater than 6
            $("#pass_show").hide();
            counter = counter + 1;
        }
        else {
            $("#pass_show").html("password is invalid");
            $("#pass_show").show();
            counter = counter - 1;
        }
    }
    if (Cpass !== "" || Cpass !== null) {
        if (pass === Cpass) {                         // check the confirm password 
            $("#Cpass_show").hide();
            counter = counter + 1;
        }
        else {
            $("#Cpass_show").html("confirm password is invalid");
            $("#Cpass_show").show();
            counter = counter - 1;
        }
    }
    if (PNo !== "" || PNo !== null) {
        if (phoneexp.test(PNo)) {                   // check the phone number is us format
            $("#phoneNo_show").hide();
            counter = counter + 1;
        }
        else {
            $("#phoneNo_show").html("phone Number is invalid");
            $("#phoneNo_show").show();
            counter = counter - 1;
        }
    }
    if (dat !== "" || dat !== null) {             //check the date is valid or not

        if (datexp.test(dat)) {
            if (!(isFutureDate(dat))) {
                $("#date_birth").hide();
                counter = counter + 1;
            }
            else {
                $("#date_birth").html("you entered future date");
                $("#date_birth").show();
                counter = counter - 1;
            }
        }
        else {
            $("#date_birth").html("plz enter the date mm-dd-yyyy");
            $("#date_birth").show();
            counter = counter - 1;
        }
    }
    if (counter === 8)
        $("#result").show();
    else
        $("#result").hide();
    //check the date is future or not only enter date is less than current date
    function isFutureDate(idate) {
        var today = new Date().getTime(),
                idate = idate.split("-");
        idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();
        return (today - idate) < 0 ? true : false;
    }

}  